FROM ubuntu:20.04
RUN DEBIAN_FRONTEND=noninteractive apt-get update
RUN DEBIAN_FRONTEND=noninteractive apt-get install -y nginx php-fpm supervisor
RUN rm -rf /etc/supervisor/*
RUN rm -rf /etc/nginx/sites-enabled/*
COPY assets/supervisord.conf /etc/supervisor/supervisord.conf
COPY assets/services.conf /etc/supervisor/conf.d/services.conf
COPY assets/sites.conf /etc/nginx/sites-enabled/default.conf

CMD ["supervisord"]
